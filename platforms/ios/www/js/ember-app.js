window.App = Ember.Application.create({
  currentPath: ''
});

App.ApplicationView = Ember.View.extend({
  didInsertElement: function() {
  }
});

App.authenticateUser = function() {
  var oauthPlugin = cordova.require("com.salesforce.plugin.oauth");

  return new Ember.RSVP.Promise(function(resolve, reject) {
    oauthPlugin.getAuthCredentials(function(creds) {
        // Create forcetk client instance for rest API calls
        var forceClient = new forcetk.Client();
        forceClient.setSessionToken(creds.accessToken, "v31.0", creds.instanceUrl);
        App.client = forceClient

        resolve(forceClient);
      }, function (error) {
        reject(error);
      }
    );
  });
}

App.ApplicationAdapter = DS.FIXTUREAdapter;

App.ApplicationController = Ember.Controller.extend({
  updateCurrentPath: function() {
    App.set('currentPath', this.get('currentPath'));
  }.observes('currentPath')
});

App.Router.map(function() {
  this.route('login', {path: '/'});
  this.route('users', {path: '/users/'});
  this.route('user', {path: '/users/:user_id'});
  this.route('customers', {path: '/customers/'});
  this.route('customer', {path: '/customers/:customer_id'});
});

App.LoginRoute = Ember.Route.extend({
  beforeModel: function(transition) {
    var route = this;
    App.authenticateUser().then(function(){
      route.transitionTo('users');
    }, function(error){
      alert(error);
    });
  }
});

App.UsersRoute = Ember.Route.extend({
  setupController: function(controller) {
    controller.fetchUsers();
  }
});

App.UserRoute = Ember.Route.extend({
  model: function(params) {
    return this.store.getById('user', params.user_id);
  },
  setupController: function(controller, model) {
    this.controllerFor('header').set('previousRoute', 'users');
    controller.set('user', model);
  }
});

App.CustomersRoute = Ember.Route.extend({
  setupController: function(controller) {
    controller.fetchCustomers();
  }
});

App.CustomerRoute = Ember.Route.extend({
  model: function(params) {
    return this.store.getById('customer', params.customer_id);
  },
  setupController: function(controller, model) {
    this.controllerFor('header').set('previousRoute', 'customers');
    controller.set('customer', model);
  }
});



App.HeaderController = Ember.ArrayController.extend({
  previousRoute: 'users',
  needs: ['application'],
  current_path: Ember.computed.alias('controllers.application.currentPath'),
  showBackButton: false,
  title: 'users',

  actions: {
    previousPage: function() {
      this.transitionToRouteAnimated(this.get('previousRoute'), {main: 'slideRight'});
    }
  },

  showHomeButton: function() {
    if (this.get('current_path') === 'users' || this.get('current_path') === 'customers') {
      this.set('showBackButton', false)
    } else {
      this.set('showBackButton', true)
    }

    this.set('title', this.get('current_path').capitalize());
  }.observes('current_path')

});

App.HeaderView = Ember.View.extend({
  didInsertElement: function() {
    this.controller.showHomeButton()
  }
});

App.UserView = Ember.View.extend({
});

App.UsersView = Ember.View.extend({
});

App.CustomerView = Ember.View.extend({
});

App.CustomersView = Ember.View.extend({
});


App.UsersController = Ember.ArrayController.extend({
  users: [],

  actions: {
    goToUser: function(user) {
      this.transitionToRouteAnimated('user', {main: 'slideLeft'}, user);
    }
  },

  fetchUsers: function() {
    var controller = this;

    var soql = 'SELECT Id, Name, Email, FullPhotoUrl, Phone, Title FROM User';
    App.client.query(soql, function(data){
      var users = data.records;

      for (var i = 0; i < users.length; i++) {
        var user = users[i];

        controller.store.push('user', {
          id: user.Id,
          name: user.Name,
          email: user.Email,
          photoUrl: user.FullPhotoUrl,
          phone: user.Phone,
          title: user.Title
        });
      };

      controller.set('users', controller.store.all('user'));
    }, function(error){
      alert('Failed to fetch users: ' + error);
    });

  }
});

App.UserController = Ember.ArrayController.extend({
  user: null
});

App.CustomersController = Ember.ArrayController.extend({
  customers: [],
  showLoginModal: false,
  email: '',
  password: '',

  actions: {
    goToCustomer: function(customer) {
      this.transitionToRouteAnimated('customer', {main: 'slideLeft'}, customer);
    },

    login: function() {
      var controller = this;
      var email = escape(controller.encodeUrl(controller.get('email')));
      var password = escape(controller.encodeUrl(controller.get('password')));

      Em.$.ajax({
        type: "POST",
        url: "http://dev.payliquid.com/merchant_api/login",
        contentType: 'application/x-www-form-urlencoded',
        data: 'username='+email+'&password='+password,
        beforeSend: function(xhr) {
          // App.delete_cookie("CSRF")
          // xhr.setRequestHeader("CSRF", App.generateCSRF())
        },
        success: function(response, status, xhr) {
          // App.delete_cookie("CSRF")
          // setupApplication(controller, router);
          controller.fetchCustomers();
        },
        error: function(xhr, ajaxOptions, thrownError) {
          if (xhr.status == 401) {
            alert('unauthorized')
          } else {
            // console.log("There was an error :" + xhr.status);
          }
        }
      });
    }
  },

  fetchCustomers: function() {
    var controller = this;

    Em.$.ajax({
      type: "GET",
      dataType: "json",
      url: "http://dev.payliquid.com/merchant_api/customers/index.json?pageSize=10&page=0&deleted=false",
      // beforeSend: function(xhr) {
        // xhr.setRequestHeader("CSRF", App.generateCSRF())
      // }
      success: function(data) {
        Em.$('#myModalexample').removeClass('active');

        var customers = data.results;
        for (var i = 0; i < customers.length; i++) {
          var customer = customers[i];
          delete customer.address
          customer.id = customer.uuid

          controller.store.push('customer', customer);
        }
        controller.set('customers', controller.store.all('customer'))
      },
      error: function(xhr, error, status) {
        if (xhr.status == 401) {
          Em.$('#myModalexample').addClass('active');
        } else {
          console.log(xhr);
        }
      }
    });
  },

  encodeUrl: function(string) {
    string = string.replace(/\r\n/g,"\n"); 
    var utftext = ""; 
 
    for (var n = 0; n < string.length; n++) { 
 
      var c = string.charCodeAt(n); 
 
      if (c < 128) { 
        utftext += String.fromCharCode(c); 
      } 
      else if((c > 127) && (c < 2048)) { 
        utftext += String.fromCharCode((c >> 6) | 192); 
        utftext += String.fromCharCode((c & 63) | 128); 
      } 
      else { 
        utftext += String.fromCharCode((c >> 12) | 224); 
        utftext += String.fromCharCode(((c >> 6) & 63) | 128); 
        utftext += String.fromCharCode((c & 63) | 128); 
      } 
 
    } 
 
    return utftext; 
  }
});

App.CustomerController = Ember.ArrayController.extend({
  customer: null
});

App.User = DS.Model.extend({
  name: DS.attr(),
  email: DS.attr(),
  photoUrl: DS.attr(),
  phone: DS.attr(),
  title: DS.attr()
});

App.Customer = DS.Model.extend({
  businessName: DS.attr(),
  deleted: DS.attr(),
  email: DS.attr(),
  firstname: DS.attr(),
  lastname: DS.attr(),
  name: function(){
    return this.get('firstname') + ' ' + this.get('lastname')
  }.property('firstname', 'lastname'),
  merchantReference: DS.attr(),
  optedIn: DS.attr(),
  phoneHome: DS.attr(),
  phoneMobile: DS.attr(),
  phoneWork: DS.attr(),
  territory: DS.attr(),
  territoryUuid: DS.attr(),
  uuid: DS.attr()
});


App.deferReadiness();

document.addEventListener('deviceready', function() {
  App.advanceReadiness();
});